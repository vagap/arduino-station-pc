/*
 * File:   mainForm.cpp
 * Author: V@G@P
 *
 * Created on 7 Октябрь 2014 г., 1:23
 */

#include "mainForm.hpp"
#include <QVector>

mainForm::mainForm()
{
	widget.setupUi(this);

	connect(widget.connectButton, SIGNAL(clicked()), this, SLOT(connecting()));
	connect(PCFeatureLibrary::FeatureManager::instance(), SIGNAL(newMeteoData(uint8_t, uint8_t)), this, SLOT(newMeteoData(uint8_t, uint8_t)));

	int32_t curIndex;
	QVector<QString> ListPorts;
	PCFeatureLibrary::FeatureManager::instance()->getListNamePorts(ListPorts, curIndex);

	foreach (QString port, ListPorts) {
		widget.portComboBox->addItem(port);
	}
	widget.portComboBox->setCurrentIndex(curIndex);

	QVector<int32_t> ListRate;
	PCFeatureLibrary::FeatureManager::instance()->getListRate(ListRate, curIndex);

	for (int32_t i = 0; i < ListRate.size(); ++i) {
		widget.rateComboBox->addItem(QString("%1").arg(ListRate[i]), ListRate[i]);
	}
	widget.rateComboBox->setCurrentIndex(curIndex);
}

mainForm::~mainForm()
{
	PCFeatureLibrary::FeatureManager::deinstance();
}

void mainForm::connecting()
{
	PCFeatureLibrary::FeatureManager::instance()->setSettings(widget.portComboBox->currentText(), widget.rateComboBox->currentData().toInt());
	PCFeatureLibrary::FeatureManager::instance()->connectPort();
}

void mainForm::newMeteoData(uint8_t temperature, uint8_t humidity)
{
	widget.TemperatureLCD->display(temperature);
	widget.HumidityLCD->display(humidity);
}