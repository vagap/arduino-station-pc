/* 
 * File:   mainForm.hpp
 * Author: V@G@P
 *
 * Created on 7 Октябрь 2014 г., 1:23
 */

#pragma once

#include "ui_mainForm.h"
#include "FeatureManager.hpp"

class mainForm : public QDialog {
	Q_OBJECT
public:
	mainForm();
	virtual ~mainForm();

public slots:
	void connecting();
	void newMeteoData(uint8_t temperature, uint8_t humidity);

private:
	Ui::mainForm widget;
};
