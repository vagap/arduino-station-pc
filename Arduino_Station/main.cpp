 /* 
 * File:   main.cpp
 * Author: V@G@P
 *
 * Created on 7 Октябрь 2014 г., 1:22
 */

#include <QApplication>
#include "mainForm.hpp"

int main(int argc, char *argv[])
{
	// initialize resources, if needed
	// Q_INIT_RESOURCE(resfile);

	QApplication app(argc, argv);

	mainForm form;
	form.show();

	return app.exec();
}
