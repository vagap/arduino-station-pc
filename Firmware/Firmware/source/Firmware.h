//-------------------------------------------------------------------
#ifndef __firmware_H__
#define __firmware_H__
//-------------------------------------------------------------------

#include "Arduino.h"
#include <DFR0107.h>
#include <dht11.h>

#include "../../../PCFeatureLibrary/EnumHeader.h"

//-------------------------------------------------------------------

enum { TILT_SWITCH = 2u, IR_PIN = 5u, DHT11_PIN = 7u };
enum { TIME_DHT11_REQUEST = 3000, UART_RATE = 9600 };

void funct();

//===================================================================
// -> DO NOT WRITE ANYTHING BETWEEN HERE...
// 		This section is reserved for automated code generation
// 		This process tries to detect all user-created
// 		functions in main_sketch.cpp, and inject their  
// 		declarations into this file.
// 		If you do not want to use this automated process,  
//		simply delete the lines below, with "&MM_DECLA" text 
//===================================================================
//---- DO NOT DELETE THIS LINE -- @MM_DECLA_BEG@---------------------
//---- DO NOT DELETE THIS LINE -- @MM_DECLA_END@---------------------
// -> ...AND HERE. This space is reserved for automated code generation!
//===================================================================


//-------------------------------------------------------------------


#endif
//-------------------------------------------------------------------
