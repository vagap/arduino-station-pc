/*************************************************************
project: <type project name here>
author: <type your name here>
description: <type what this file does>
*************************************************************/

#include "Firmware.h"

DRF0107 IR(IR_PIN);
dht11 DHT11(DHT11_PIN);

unsigned long timeLastReqeustDHT11 = 0, timeLastButtonEvent = 0;
uint8_t tiltSwitchStatus;

void setup()
{                
	pinMode(IR_PIN, INPUT);
	digitalWrite(IR_PIN, LOW); 
	pinMode(DHT11_PIN, INPUT);
	digitalWrite(DHT11_PIN, LOW); 

	Serial.begin(UART_RATE);

	tiltSwitchStatus = digitalRead(TILT_SWITCH);
	timeLastButtonEvent = millis();
}

void funct()
{
	unsigned long mill = millis();

	if (mill - timeLastReqeustDHT11 > TIME_DHT11_REQUEST) {
		unsigned char buf = PCFeatureLibrary::FIRST_MESSAGE_BYTE;
		if (DHT11.read() == 0) {
			Serial.write(buf);
			buf = PCFeatureLibrary::GET_METEO_DATA;
			Serial.write(buf);
			buf = DHT11.temperature;
			Serial.write(buf);
			buf = DHT11.humidity;
			Serial.write(buf);
			buf = PCFeatureLibrary::LAST_MESSAGE_BYTE;
			Serial.write(buf);
		}
		timeLastReqeustDHT11 = mill;
	}

	if ((mill - timeLastButtonEvent > TIME_DHT11_REQUEST) && (tiltSwitchStatus != digitalRead(TILT_SWITCH))) {
		unsigned char buf = PCFeatureLibrary::FIRST_MESSAGE_BYTE;
		Serial.write(buf);
		buf = PCFeatureLibrary::CHANGE_ORIENTATION;
		Serial.write(buf);
		buf = tiltSwitchStatus == HIGH? PCFeatureLibrary::HORIZONTAL: PCFeatureLibrary::VERTICAL;
		Serial.write(buf);
		buf = PCFeatureLibrary::LAST_MESSAGE_BYTE;
		Serial.write(buf);
		tiltSwitchStatus = !tiltSwitchStatus;
		timeLastButtonEvent = mill;
	}
}

void loop()
{
	enum BUTTON but = IR.get_ir_key(&funct);
	if (but != BUTTON_ERROR) {
		unsigned char buf = PCFeatureLibrary::FIRST_MESSAGE_BYTE;
		Serial.write(buf);
		buf = PCFeatureLibrary::IR_REMOTE_SIGNAL;
		Serial.write(buf);

		switch (but) {
			case BUTTON_POWER:
				buf = PCFeatureLibrary::BUTTON_POWER;
				break;
			case BUTTON_STOP:
				buf = PCFeatureLibrary::BUTTON_STOP;
				break;
			case BUTTON_LEFT:
				buf = PCFeatureLibrary::BUTTON_PRED;
				break;
			case BUTTON_RIGHT:
				buf = PCFeatureLibrary::BUTTON_NEXT;
				break;
			case BUTTON_PAUSE:
				buf = PCFeatureLibrary::BUTTON_PLAY_PAUSE;
				break;
			case BUTTON_VOL_PLUS:
				buf = PCFeatureLibrary::BUTTON_VOLUME_UP;
				break;
			case BUTTON_VOL_MINUS:
				buf = PCFeatureLibrary::BUTTON_VOLUME_DOWN;
				break;
			case BUTTON_UP:
				buf = PCFeatureLibrary::BUTTON_UP;
				break;
			case BUTTON_DOWN:
				buf = PCFeatureLibrary::BUTTON_DOWN;
				break;
			case BUTTON_EQ:
				buf = PCFeatureLibrary::BUTTON_EQ;
				break;
			case BUTTON_REPT:
				buf = PCFeatureLibrary::BUTTON_REPT;
				break;
			case BUTTON_0:
				buf = PCFeatureLibrary::BUTTON_0;
				break;
			case BUTTON_1:
				buf = PCFeatureLibrary::BUTTON_1;
				break;
			case BUTTON_2:
				buf = PCFeatureLibrary::BUTTON_2;
				break;
			case BUTTON_3:
				buf = PCFeatureLibrary::BUTTON_3;
				break;
			case BUTTON_4:
				buf = PCFeatureLibrary::BUTTON_4;
				break;
			case BUTTON_5:
				buf = PCFeatureLibrary::BUTTON_5;
				break;
			case BUTTON_6:
				buf = PCFeatureLibrary::BUTTON_6;
				break;
			case BUTTON_7:
				buf = PCFeatureLibrary::BUTTON_7;
				break;
			case BUTTON_8:
				buf = PCFeatureLibrary::BUTTON_8;
				break;
			case BUTTON_9:
				buf = PCFeatureLibrary::BUTTON_9;
				break;
			default:
				break;
		}
		Serial.write(buf);
		buf = PCFeatureLibrary::LAST_MESSAGE_BYTE;
		Serial.write(buf);
	}
}
