/* 
 * File:   DeviceManager.hpp
 * Author: V@G@P
 *
 * Created on 3 Октябрь 2014 г., 11:34
 */

#pragma once

#include "UARTPort.hpp"
#include "EnumHeader.h"

#include <memory>
#include <QThread>
#include <QVector>

namespace PCFeatureLibrary
{
	class DeviceManager: public QObject
	{
		Q_OBJECT
	private:
		DeviceManager();
		~DeviceManager();

		std::shared_ptr<UARTPort> m_port;
		std::shared_ptr<QThread> m_thread;

		static DeviceManager *device;

		void decode(const QByteArray &command);

	public:
		static DeviceManager* instance();
		static void deinstance();
	signals:
		void changeOrientation(PCFeatureLibrary::ScreenOrientation oriental);
		void newMeteoData(uint8_t temperature, uint8_t humidity);
		void IRRemote(PCFeatureLibrary::IRRemoteButton button);
		void error(QString textError);
		void connecting();
		void setSettings(const QString &name, QSerialPort::BaudRate baudRate);

	public slots:
		void connectPort(const QString &name, QSerialPort::BaudRate baudRate);
		void getData(QByteArray data);
	};
}
