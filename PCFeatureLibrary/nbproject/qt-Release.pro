# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = lib
DESTDIR = dist/Release/MinGW-Windows
TARGET = PCFeatureLibrary
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += staticlib release 
PKGCONFIG +=
QT = core gui
SOURCES += DeviceManager.cpp FeatureManager.cpp PCFeatureLibrary.cpp UARTPort.cpp
HEADERS += DeviceManager.hpp EnumHeader.h FeatureManager.hpp PCFeatureLibrary.hpp UARTPort.hpp
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/MinGW-Windows
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += 
LIBS += 
equals(QT_MAJOR_VERSION, 4) {
QMAKE_CXXFLAGS += -std=c++11
}
equals(QT_MAJOR_VERSION, 5) {
CONFIG += c++11
}
