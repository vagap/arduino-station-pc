/* 
 * File:   EnumHeader.h
 * Author: V@G@P
 *
 * Заголовочник с определениями необходимыми для общения с arduino
 * 
 * Created on 4 Октябрь 2014 г., 13:55
 */

#ifndef ENUMHEADER_H
#define	ENUMHEADER_H

namespace PCFeatureLibrary
{

typedef enum { CHANGE_ORIENTATION = 0, IR_REMOTE_SIGNAL = 1, GET_METEO_DATA = 2 } TypeMessage;
typedef enum { HORIZONTAL = 0, VERTICAL = 1 } ScreenOrientation;
typedef enum {	BUTTON_POWER = 0,
		BUTTON_VOLUME_DOWN = 1,
		BUTTON_VOLUME_UP = 2,
		BUTTON_STOP = 3,
		BUTTON_PLAY_PAUSE = 4,
		BUTTON_PRED = 5,
		BUTTON_NEXT = 6,
		BUTTON_UP = 7,
		BUTTON_DOWN = 8,
		BUTTON_EQ = 9,
		BUTTON_REPT = 10,
		BUTTON_0 = 11,
		BUTTON_1 = 12,
		BUTTON_2 = 13,
		BUTTON_3 = 14,
		BUTTON_4 = 15,
		BUTTON_5 = 16,
		BUTTON_6 = 17,
		BUTTON_7 = 18,
		BUTTON_8 = 19,
		BUTTON_9 = 20 } IRRemoteButton;

enum { FIRST_MESSAGE_BYTE = 0xFE, LAST_MESSAGE_BYTE = 0xFF };

}

#endif	/* ENUMHEADER_H */

