#include "PCFeatureLibrary.hpp"

#include <cstdint>
#include <windows.h>
#include <mmdeviceapi.h>

namespace PCFeatureLibrary
{
	void ScreenRotation(ScreenOrientation oriental)
	{
		DEVMODE devMode;
		if (EnumDisplaySettings( NULL, ENUM_CURRENT_SETTINGS, &devMode ) == 0)
			return;

		uint8_t needOriental = (oriental == HORIZONTAL)? DMDO_DEFAULT: DMDO_90;
		if (devMode.dmDisplayOrientation == needOriental)
			return;
		else
			devMode.dmDisplayOrientation = needOriental;

		int tempValue = devMode.dmPelsWidth;
		devMode.dmPelsWidth = devMode.dmPelsHeight;
		devMode.dmPelsHeight = tempValue;

		ChangeDisplaySettings(&devMode, 0);
	}

	void GenerateKeyEvent(Qt::Key key)
	{
		uint32_t KeyCode;
		switch (key) {
		case Qt::Key_VolumeDown:
			KeyCode = VK_VOLUME_DOWN;
			break;
		case Qt::Key_VolumeUp:
			KeyCode = VK_VOLUME_UP;
			break;
		case Qt::Key_MediaStop:
			KeyCode = VK_MEDIA_STOP;
			break;
		case Qt::Key_MediaPlay:
			KeyCode = VK_MEDIA_PLAY_PAUSE;
			break;
		case Qt::Key_MediaPrevious:
			KeyCode = VK_MEDIA_PREV_TRACK;
			break;
		case Qt::Key_MediaNext:
			KeyCode = VK_MEDIA_NEXT_TRACK;
			break;
		case Qt::Key_PageUp:
			KeyCode = VK_PRIOR;
			break;
		case Qt::Key_PageDown:
			KeyCode = VK_NEXT;
			break;
		case Qt::Key_VolumeMute:
			KeyCode = VK_VOLUME_MUTE;
			break;
		case Qt::Key_Space:
			KeyCode = VK_SPACE;
			break;
		case Qt::Key_Enter:
		case Qt::Key_Return:
			KeyCode = VK_RETURN;
			break;
		case Qt::Key_Left:
			KeyCode = VK_LEFT;
			break;
		case Qt::Key_Right:
			KeyCode = VK_RIGHT;
			break;
		case Qt::Key_Up:
			KeyCode = VK_UP;
			break;
		case Qt::Key_Down:
			KeyCode = VK_DOWN;
			break;
		default:
			KeyCode = 0;
			break;
		};

		if (KeyCode != 0) {
			::keybd_event(KeyCode, 0, KEYEVENTF_EXTENDEDKEY, reinterpret_cast<ULONG_PTR>(nullptr));
			::keybd_event(KeyCode, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, reinterpret_cast<ULONG_PTR>(nullptr));
		}
	}

	QVector<QString> GetListComPorts()
	{
		QVector<QString> result;
		HKEY hKey;
		QString pathListPorts("HARDWARE\\DEVICEMAP\\SERIALCOMM");
		const DWORD maxBufferSize = 16383;

		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, pathListPorts.toStdWString().c_str(), 0, KEY_READ, &hKey) != ERROR_SUCCESS)
			return result;

		DWORD index = 0, type = REG_SZ, size = maxBufferSize;
		wchar_t nameSubKey[maxBufferSize], value[maxBufferSize];
		while (RegEnumValue(hKey, index++, nameSubKey, &(size = maxBufferSize), 0, NULL, NULL, NULL) == ERROR_SUCCESS) {
			if (RegQueryValueEx(hKey, nameSubKey, NULL, &type, (LPBYTE)&value, &(size = maxBufferSize)) == ERROR_SUCCESS)
				result.push_back(QString::fromWCharArray(value));
		}

		return result;
	}
}
