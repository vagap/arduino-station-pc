#include "UARTPort.hpp"

namespace PCFeatureLibrary
{

UARTPort::UARTPort()
{
	thisPort.setDataBits(QSerialPort::Data8);
	thisPort.setParity(QSerialPort::NoParity);
	thisPort.setStopBits(QSerialPort::OneStop);
	thisPort.setFlowControl(QSerialPort::NoFlowControl);
}

void UARTPort::moveToThread(QThread *targetThread)
{
	thisPort.moveToThread(targetThread);
	QObject::moveToThread(targetThread);
}

void UARTPort::connectPort()
{
	if (thisPort.isOpen())
		thisPort.close();

	if (!thisPort.open(QIODevice::ReadWrite))
		emit error_("Error open");
}

void UARTPort::disconnectPort()
{
	if (thisPort.isOpen())
		thisPort.close();
}

void UARTPort::process_Port()
{
	qRegisterMetaType<QSerialPort::SerialPortError>("QSerialPort::SerialPortError");
	connect(&thisPort, SIGNAL(readyRead()), this, SLOT(readInPort()));
	connect(&thisPort, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handleError(QSerialPort::SerialPortError)));
}

void UARTPort::writeToPort(QByteArray data)
{
	if (thisPort.isOpen())
		thisPort.write(data);
}

void UARTPort::readInPort()
{
	QByteArray data;
	data.append(thisPort.readAll());
	emit outPort(data);
}

void UARTPort::setSettings(const QString &name, QSerialPort::BaudRate baudRate)
{
	thisPort.setPortName(name);
	thisPort.setBaudRate(baudRate);
}

void UARTPort::handleError(QSerialPort::SerialPortError error)
{
	if ( (thisPort.isOpen()) && (error == QSerialPort::ResourceError)) {
		error_(thisPort.errorString().toLocal8Bit());
		disconnectPort();
	}
}

}
