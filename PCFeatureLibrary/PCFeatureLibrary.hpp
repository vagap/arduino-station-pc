/* 
 * File:   MonitorManager.hpp
 * Author: V@G@P (vagap0v@yandex.ru)
 *
 * Библиотека реализации функционала ОС
 * 
 * Created on 1 Октябрь 2014 г., 16:02
 */

#pragma once

#include <QString>
#include <QVector>

#include "EnumHeader.h"

namespace PCFeatureLibrary
{
	void ScreenRotation(ScreenOrientation oriental);	// Функция поворота экрана 
	void GenerateKeyEvent(Qt::Key key);			// Функция генерация мультимедия событий (пауза и прочее)
	QVector<QString> GetListComPorts();			// Функция получения списка COM-портов
}
