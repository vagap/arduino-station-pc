#include "EnumHeader.h"
#include "PCFeatureLibrary.hpp"
#include "DeviceManager.hpp"

#include "FeatureManager.hpp"

namespace PCFeatureLibrary
{
	FeatureManager *FeatureManager::manager = nullptr;
	const QString FeatureManager::defaultName = "COM3";

	FeatureManager::FeatureManager():
		m_NameComPort(defaultName),
		m_ComRate(defaultRate),
		m_KeyMap(),
		m_RateMap()
	{
		DeviceManager::instance();
		m_KeyMap.insert(BUTTON_VOLUME_DOWN,	Qt::Key_VolumeDown);
		m_KeyMap.insert(BUTTON_VOLUME_UP,	Qt::Key_VolumeUp);
		m_KeyMap.insert(BUTTON_STOP,		Qt::Key_MediaStop);
		m_KeyMap.insert(BUTTON_PLAY_PAUSE,	Qt::Key_MediaPlay);
		m_KeyMap.insert(BUTTON_PRED,		Qt::Key_MediaPrevious);
		m_KeyMap.insert(BUTTON_NEXT,		Qt::Key_MediaNext);
		m_KeyMap.insert(BUTTON_EQ,		Qt::Key_VolumeMute);
		m_KeyMap.insert(BUTTON_UP,		Qt::Key_PageUp);
		m_KeyMap.insert(BUTTON_DOWN,		Qt::Key_PageDown);
		m_KeyMap.insert(BUTTON_0,		Qt::Key_Space);
		m_KeyMap.insert(BUTTON_5,		Qt::Key_Return);
		m_KeyMap.insert(BUTTON_4,		Qt::Key_Left);
		m_KeyMap.insert(BUTTON_6,		Qt::Key_Right);

		m_RateMap.insert(1200,		QSerialPort::Baud1200);
		m_RateMap.insert(2400,		QSerialPort::Baud2400);
		m_RateMap.insert(4800,		QSerialPort::Baud4800);
		m_RateMap.insert(9600,		QSerialPort::Baud9600);
		m_RateMap.insert(19200,		QSerialPort::Baud19200);
		m_RateMap.insert(38400,		QSerialPort::Baud38400);
		m_RateMap.insert(57600,		QSerialPort::Baud57600);
		m_RateMap.insert(115200,	QSerialPort::Baud115200);

		connect(DeviceManager::instance(), SIGNAL(changeOrientation(PCFeatureLibrary::ScreenOrientation)), this, SLOT(changeScreenOrientation(PCFeatureLibrary::ScreenOrientation)));
		connect(DeviceManager::instance(), SIGNAL(IRRemote(PCFeatureLibrary::IRRemoteButton)), this, SLOT(getIRSignal(PCFeatureLibrary::IRRemoteButton)));
		connect(DeviceManager::instance(), SIGNAL(newMeteoData(uint8_t, uint8_t)), this, SLOT(getMeteoData(uint8_t, uint8_t)));
	}

	FeatureManager* FeatureManager::instance()
	{
		if (!FeatureManager::manager)
			FeatureManager::manager = new FeatureManager();

		return FeatureManager::manager;
	}

	void FeatureManager::deinstance()
	{
		delete FeatureManager::manager;
		FeatureManager::manager = nullptr;
	}

	void FeatureManager::getListRate(QVector<int32_t> &ListRates, int32_t &currentPossition)
	{
		ListRates.clear();
		currentPossition = 0;
		int32_t index = 0;

		foreach (int32_t rate, m_RateMap.keys()) {
			if (m_RateMap.value(rate) == defaultRate)
				currentPossition = index;
			ListRates.push_back(rate);
			++index;
		}
	}

	void FeatureManager::getListNamePorts(QVector<QString> &ListNamePorts, int32_t &currentPossition)
	{
		ListNamePorts.clear();
		currentPossition = -1;
		int32_t index = 0;

		ListNamePorts = GetListComPorts();
		foreach (QString namePort, ListNamePorts) {
			if (namePort == defaultName)
				currentPossition = index;
			++index;
		}
	}

	void FeatureManager::connectPort()
	{
		DeviceManager::instance()->connectPort(m_NameComPort, m_ComRate);
	}

	void FeatureManager::setSettings(const QString &name, int32_t baudRate)
	{
		m_NameComPort = name;
		if (m_RateMap.contains(baudRate))
			m_ComRate = m_RateMap.value(baudRate);
	}

	void FeatureManager::getMeteoData(uint8_t temperature, uint8_t humidity)
	{
		emit(newMeteoData(temperature, humidity));
	}

	void FeatureManager::getIRSignal(PCFeatureLibrary::IRRemoteButton button)
	{
		if (m_KeyMap.contains(button))
			GenerateKeyEvent(m_KeyMap.value(button));
	}

	void FeatureManager::changeScreenOrientation(PCFeatureLibrary::ScreenOrientation oriental)
	{
		ScreenRotation(oriental);
	}

}
