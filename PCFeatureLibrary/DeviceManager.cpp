#include "DeviceManager.hpp"

namespace PCFeatureLibrary
{
	DeviceManager *DeviceManager::device = nullptr;

	DeviceManager::DeviceManager():
		m_port(std::shared_ptr<UARTPort>(new UARTPort())),
		m_thread(std::shared_ptr<QThread>(new QThread()))
	{
		m_port->moveToThread(m_thread.get());

		qRegisterMetaType<QSerialPort::BaudRate>("QSerialPort::BaudRate");

		connect(m_thread.get(), SIGNAL(started()), m_port.get(), SLOT(process_Port()));
		connect(m_port.get(), SIGNAL(finished_Port()), m_thread.get(), SLOT(quit()));
		connect(m_port.get(), SIGNAL(finished_Port()), m_thread.get(), SLOT(deleteLater()));
		connect(m_thread.get(), SIGNAL(finished()), m_port.get(), SLOT(deleteLater()));

		connect(this, SIGNAL(connecting()), m_port.get(), SLOT(connectPort()));
		connect(this, SIGNAL(setSettings(const QString&, QSerialPort::BaudRate)), m_port.get(), SLOT(setSettings(const QString&, QSerialPort::BaudRate)));
		connect(m_port.get(), SIGNAL(outPort(QByteArray)), this, SLOT(getData(QByteArray)));

		m_thread->start();
	}

	DeviceManager::~DeviceManager()
	{
		if (m_thread->isRunning())
			m_thread->quit();
	}

	void DeviceManager::decode(const QByteArray &command)
	{
		if (command.size() < 2)
			return;

		switch (command[0]) {
		case CHANGE_ORIENTATION:
			if (command[1] == HORIZONTAL)
				emit(changeOrientation(HORIZONTAL));
			if (command[1] == VERTICAL)
				emit(changeOrientation(VERTICAL));
			break;
		case IR_REMOTE_SIGNAL:
			if ((uint8_t)command[1] > (uint8_t)BUTTON_9)
				return;
			emit(IRRemote((IRRemoteButton)command[1]));
			break;
		case GET_METEO_DATA:
			if (command.size() != 3)
				return;
			emit(newMeteoData((uint8_t)command[1], (uint8_t)command[2]));
			break;
		}
	}

	DeviceManager* DeviceManager::instance()
	{
		if (!DeviceManager::device)
			DeviceManager::device = new DeviceManager();

		return DeviceManager::device;
	}

	void DeviceManager::deinstance()
	{
		delete DeviceManager::device;
		DeviceManager::device = nullptr;
	}

	void DeviceManager::connectPort(const QString &name, QSerialPort::BaudRate baudRate)
	{
		emit(setSettings(name, baudRate));
		emit(connecting());
	}

	void DeviceManager::getData(QByteArray data)
	{
		static QByteArray buffer;
		static bool receiveCommand = false;
		for (auto it: data) {
			if (!receiveCommand) {
				receiveCommand = ((uint8_t)it == FIRST_MESSAGE_BYTE);
				continue;
			}

			if ((uint8_t)it == LAST_MESSAGE_BYTE) {
				decode(buffer);
				buffer.clear();
				receiveCommand = false;
			} else {
				buffer.append(it);
			}
		}

	}
}
