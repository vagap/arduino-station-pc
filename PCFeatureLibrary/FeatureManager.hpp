/* 
 * File:   MonitorManager.hpp
 * Author: V@G@P (vagap0v@yandex.ru)
 *
 * Библиотека управления функционалом ОС
 *
 * Created on 21 Октябрь 2014 г., 13:28
 */

#pragma once

#include <functional>

#include <QMap>
#include <QVector>
#include <QtSerialPort\QSerialPort>

#include "EnumHeader.h"

namespace PCFeatureLibrary
{
	typedef std::function<void(int8_t, uint8_t)> meteoSlot;

	class FeatureManager: public QObject
	{
		Q_OBJECT
	private:
		FeatureManager();

		static FeatureManager *manager;

		QString m_NameComPort;
		QSerialPort::BaudRate m_ComRate;
		QMap<IRRemoteButton, Qt::Key> m_KeyMap;
		QMap<int32_t, QSerialPort::BaudRate> m_RateMap;

		const static QSerialPort::BaudRate defaultRate = QSerialPort::Baud9600;
		const static QString defaultName;

	public:
		static FeatureManager* instance();
		static void deinstance();
		void getListRate(QVector<int32_t> &ListRates, int32_t &currentPossition);
		void getListNamePorts(QVector<QString> &ListNamePorts, int32_t &currentPossition);

	public slots:
		void connectPort();
		void setSettings(const QString &name, int32_t baudRate);
		void getMeteoData(uint8_t temperature, uint8_t humidity);

	private slots:
		void getIRSignal(PCFeatureLibrary::IRRemoteButton button);
		void changeScreenOrientation(PCFeatureLibrary::ScreenOrientation oriental);

	signals:
		void newMeteoData(uint8_t temperature, uint8_t humidity);
	};
}