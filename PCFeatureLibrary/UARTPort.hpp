/* 
 * File:   UARTPort.hpp
 * Author: V@G@P (vagap0v@yandex.ru)
 *
 * Библиотека для коммуникации с UART-портом
 * 
 * Created on 1 Октябрь 2014 г., 16:00
 */

#pragma once

#include <QtSerialPort\QSerialPort>

namespace PCFeatureLibrary
{
	class UARTPort: public QObject
	{
		Q_OBJECT
	private:
		QSerialPort thisPort;

	public:
		UARTPort();
		void moveToThread(QThread *targetThread);

	signals:
		void finished_Port();
		void outPort(QByteArray data);
		void error_(QString err);

	public slots:
		void connectPort();
		void disconnectPort();
		void process_Port();
		void writeToPort(QByteArray data);
		void readInPort();
		void setSettings(const QString &name, QSerialPort::BaudRate baudRate);

	private slots:
		void handleError(QSerialPort::SerialPortError error);
	};
}
